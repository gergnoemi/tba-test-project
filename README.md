# TBA Test Project

A Spring Boot application with a graphical user interface for creating and controlling cars.

## Requirements
For building and running the application you will need:

- [JDK 1.8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
- [Maven 3](https://maven.apache.org)

## Build and Run Instructions

You can build and run the application on your local machine either from an IDE of preference, or the terminal by using the [spring-boot maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins.html#build-tool-plugins.maven) as follows:

**Step 1:** Navigate to project directory (`/tba-test-project` by default)

**Step 2:** Open a terminal window

**Step 3:** Run the following command:
```shell
mvn spring-boot:run
```
Once the application is up and running, the server is listening on the default port and the user interface can be accessed.

**Step 4:** Open a web browser and navigate to `http://localhost:8080`

You can create, control and delete cars on the provided user interface.
