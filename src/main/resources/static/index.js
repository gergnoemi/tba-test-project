const newCarBtn = document.getElementById("newCar");
const removeBtn = document.getElementById("remove");
const removeAllBtn = document.getElementById("removeAll");
const select = document.getElementById("cars");
const height = document.getElementById("autobahn").clientHeight;
const width = document.getElementById("autobahn").clientWidth;
const carHeight = 50;
const carWidth = 75;

let id = 0;
let carsMap = new Map();

class Car {

  constructor(id, direction) {
    this.id = id;
    this.direction = direction;
    this.intervalID = 0;
  }

  // continuous animation of <car>
  moveCar() {
    this.intervalID = setInterval(this.handleCar(document.getElementById(this.id)), 100);
  }

  // sets the direction of <car>
  setDirection(direction) {
    let carObj = {
      direction: direction
    }

    fetch('http://localhost:8080/api/car/' + this.id, {
    method: "PATCH",
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(carObj)
    })
    .then(response => response.json())
    .then(console.log);
    
    this.direction = direction;
  }

  // moves <car> one unit in <direction>
  handleCar(car) {
    // <car> automatically takes a left turn
    // whenever it reaches the edge of the autobahn
    return () => {
      if (this.direction === 0) {
        // go north
        if (car.offsetTop > 5){
          car.style.top = (car.offsetTop - 5) + "px";
        } else {
          this.setDirection(1);
        }
      }
    
      if(this.direction === 1){
        // go west
        if (car.offsetLeft > 2){
          car.style.left = (car.offsetLeft - 2) + "px";
        } else {
          this.setDirection(2);
        }
      }
      
      if(this.direction === 2){
        let maxHeight = height-5-carHeight;
        // go south
        if (car.offsetTop < maxHeight){
          car.style.top = (car.offsetTop + 5) + "px";
        } else {
          this.setDirection(3);
        }
      }
      
      if(this.direction === 3){
        let maxWidth = width-2-carWidth;
        // go east
        if (car.offsetLeft < maxWidth){
          car.style.left = (car.offsetLeft + 2) + "px";
        } else {
          this.setDirection(0);
        }
      }
    };
  }

  // stops <car> animation
  destroy() {
    clearInterval(this.intervalID);
  }

}

newCarBtn.addEventListener("click", () => {
  // chooses a random position on the autobahn
  // for the new car
  let randY = (Math.random() * (height-50)) + 1;
  let randX = (Math.random() * (width-75)) + 1;

  // creates a new car element
  // and sets its attributes
  let newCar = document.createElement('div');
  id++;
  newCar.setAttribute("id", id);
  newCar.innerHTML = "Car no. " + id;
  newCar.className = "car";
  newCar.style.right = randX + "px";
  newCar.style.top =  randY + "px";

  // adds new car to the autobahn
  document.getElementById("autobahn").appendChild(newCar);

  // adds new car option to dropdown list
  let option = document.createElement('option');
  option.value = id;
  option.innerHTML = "Car no. " + id;
  select.appendChild(option);

  // creates new Car instance
  let car = new Car(id, 0);
  carsMap.set(id, car);
  // starts car
  car.moveCar();

  // creates object to be sent
  let xVal = newCar.offsetLeft;
  let yVal = height - newCar.offsetTop;
  let carObj = {
    id: id,
    direction: 0,
    startX: xVal,
    startY: yVal,
  };

  fetch('http://localhost:8080/api/car', {
    method: "POST",
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(carObj)
  })
  .then(response => response.json())
  .then(console.log);

});


// event of direction changing buttons
function changeDirection(direction) {
  switch (direction.id) {
    case("north") : {
      carsMap.get(+select.value).setDirection(0);
      break;
    }
    case("west") : {
      carsMap.get(+select.value).setDirection(1);
      break;
    }
    case("south") : {
      carsMap.get(+select.value).setDirection(2);
      break;
    }
    case("east") : {
      carsMap.get(+select.value).setDirection(3);
    }
  }
}

removeBtn.addEventListener("click", () => {
  let id = select.value;
  fetch('http://localhost:8080/api/car/' + id, {
    method: "DELETE"
    })
    .then(response => response.json())
    .then(console.log);
  
  // the unary operator converts id to number type
  let car = carsMap.get(+id);
  carsMap.delete(+id);
  // stops animation
  car.destroy();
  // removes element
  document.getElementById(id).remove();
  // removes option
  select.remove(select.selectedIndex);
})

removeAllBtn.addEventListener("click", () => {
  fetch('http://localhost:8080/api/car', {
    method: "DELETE"
    })
    .then(response => response.json())
    .then(console.log);

  // stops animation
  // and removes elements
  for (const [key, value] of carsMap) {
    value.destroy();
    document.getElementById(key).remove();
  }

  carsMap.clear();

  // removes drop-down menu options
  select.innerHTML = '';
})