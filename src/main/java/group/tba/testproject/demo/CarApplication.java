package group.tba.testproject.demo;

import java.io.*;
import java.net.Socket;

public class CarApplication {
    private int id;
    private int direction;
    private int startX;
    private int startY;
    private Socket connection;

    public static void main(String[] args) {
        CarApplication car = new CarApplication();

        // sets Car attributes with given arguments
        if (args.length > 3) {
            car.setId(Integer.parseInt(args[0]));
            car.setDirection(Integer.parseInt(args[1]));
            car.setStartX(Integer.parseInt(args[2]));
            car.setStartY(Integer.parseInt(args[3]));

            try (Socket socket = new Socket("localhost", 8081);
                 BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 PrintStream ps = new PrintStream(socket.getOutputStream(), true))
            {
                // connects to the server
                car.setConnection(socket);
                // listens to incoming messages
                // until triggered to exit
                String[] msg = br.readLine().split(" ");

                do {
                    switch (msg[0]) {
                        // switches on request-type
                        case "GET": {
                            ps.println(car.toString());
                            break;
                        }
                        case "PATCH": {
                            switch (msg[1]) {
                                // switches on attribute-type
                                case "DIRECTION": {
                                    car.setDirection(Integer.parseInt(msg[2]));
                                }
                            }
                            break;
                        }
                    }
                    msg = br.readLine().split(" ");
                } while (! "EXIT".equals(msg[0]));
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    private int getDirection() {
        return direction;
    }

    private int getStartX() {
        return startX;
    }

    private void setStartX(int startX) {
        this.startX = startX;
    }

    private int getStartY() {
        return startY;
    }

    private void setStartY(int startY) {
        this.startY = startY;
    }

    private void setDirection(int direction) {
        this.direction = direction;
    }

    private Socket getConnection() {
        return connection;
    }

    private void setConnection(Socket connection) {
        this.connection = connection;
    }

    @Override
    public String toString() {
        return id + " " + direction + " " + startX + " " + startY;
    }
}