package group.tba.testproject.demo.entity;

import org.springframework.boot.context.properties.ConstructorBinding;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    private long id;
    @Column(name = "direction")
    private int direction;
    @Column(name = "startX")
    private int startX;
    @Column(name = "startY")
    private int startY;

    public Car() {

    }

    public Car(long id, int direction, int startX, int startY) {
        this.id = id;
        this.direction = direction;
        this.startX = startX;
        this.startY = startY;
    }

    public long getId() {
        return id;
    }

    public int getDirection() {
        return direction;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    @Override
    public String toString() {
        return "Car [id=" + id + ", direction=" + direction + ", startX=" + startX + ", startY=" + startY + "]";
    }

    public String data() {
        return id + " " + direction + " " + startX + " " + startY;
    }
}
