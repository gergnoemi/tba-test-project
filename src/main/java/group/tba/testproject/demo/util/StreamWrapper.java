package group.tba.testproject.demo.util;

import java.io.BufferedReader;
import java.io.PrintStream;

public class StreamWrapper {
    public PrintStream printStream;
    public BufferedReader bufferedReader;

    public StreamWrapper(PrintStream printStream, BufferedReader bufferedReader) {
        this.printStream = printStream;
        this.bufferedReader = bufferedReader;
    }
}
