package group.tba.testproject.demo.util;

import java.io.PrintStream;

public class DeleteRequestThread implements Runnable {

    private StreamWrapper streamWrapper;

    public DeleteRequestThread(StreamWrapper streamWrapper) {
        this.streamWrapper = streamWrapper;
    }

    @Override
    public void run() {
        PrintStream ps = streamWrapper.printStream;
        ps.println("EXIT");
    }
}
