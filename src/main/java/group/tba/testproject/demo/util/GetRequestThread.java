package group.tba.testproject.demo.util;
import group.tba.testproject.demo.entity.Car;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.concurrent.Callable;

public class GetRequestThread implements Callable<Car> {

    private StreamWrapper streamWrapper;

    public GetRequestThread(StreamWrapper streamWrapper) {
        this.streamWrapper = streamWrapper;
    }

    @Override
    public Car call() throws Exception {
        BufferedReader br = streamWrapper.bufferedReader;
        PrintStream ps = streamWrapper.printStream;

        try {
            ps.println("GET");
            String[] msg = br.readLine().split(" ");
            long id = Long.parseLong(msg[0]);
            int direction = Integer.parseInt(msg[1]);
            int xValue = Integer.parseInt(msg[2]);
            int yValue = Integer.parseInt(msg[3]);
            // return Car object
            return new Car(id, direction, xValue, yValue);
        } catch(IOException e) {
            e.printStackTrace();
            throw e;
        }

    }

}
