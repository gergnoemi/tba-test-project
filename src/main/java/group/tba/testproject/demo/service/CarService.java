package group.tba.testproject.demo.service;

import group.tba.testproject.demo.util.DeleteRequestThread;
import group.tba.testproject.demo.util.GetRequestThread;
import group.tba.testproject.demo.util.StreamWrapper;
import group.tba.testproject.demo.entity.Car;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.*;

@Service
public class CarService {

    private ServerSocket serverSocket = null;
    private final int port = 8081;
    private HashMap<Long, StreamWrapper> clientStreams;

    public CarService() {
        try {
            // compiles Car application
            Process pr = Runtime.getRuntime().exec("javac .\\src\\main\\java\\group\\tba\\testproject\\demo\\CarApplication.java");
            //waits for the compiler to finish
            pr.waitFor();
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            // creates a server socket
            serverSocket = new ServerSocket(port);
        } catch (IOException ex) {
            System.err.println("ERROR: creating socket on port " + port + "\n" + ex);
            System.exit(1);
        }
        clientStreams = new HashMap<>();
    }

    public void newCarInstance(Car car) {
        // synchronize the block
        // so that concurrent calls won't have
        // their sockets messed up
        synchronized (this) {
            try {
                // creates a new Car process with given arguments
                Runtime.getRuntime().exec("java -cp .\\src\\main\\java group.tba.testproject.demo.CarApplication " + car.data());

                try {
                    // accepts new connection
                    Socket _socket = serverSocket.accept();
                    // saves the socket stream of client process
                    BufferedReader br = new BufferedReader(new InputStreamReader(_socket.getInputStream()));
                    PrintStream ps = new PrintStream(_socket.getOutputStream(), true);
                    clientStreams.put(car.getId(), new StreamWrapper(ps, br));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                System.err.println("ERROR creating new process");
                e.printStackTrace();
            }
        }
    }

    public Car getCar(long id) throws Exception {
        try {
            // if Car with given id exists
            if (clientStreams.containsKey(id)) {
                PrintStream ps = clientStreams.get(id).printStream;
                // sends a GET message to client process
                ps.println("GET");
                // reads response from client process
                BufferedReader br = clientStreams.get(id).bufferedReader;
                String[] msg = br.readLine().split(" ");
                long _id = Long.parseLong(msg[0]);
                int direction = Integer.parseInt(msg[1]);
                int xValue = Integer.parseInt(msg[2]);
                int yValue = Integer.parseInt(msg[3]);
                // returns Car object
                return new Car(_id, direction, xValue, yValue);
            }
            // otherwise throw error
            throw new NoSuchElementException();
        } catch(IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public List<Car> getCars() throws Exception {
        int length = clientStreams.size();
        if (length != 0) {
            ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(length);
            List<Future<Car>> futures = new ArrayList<>();

            for (Map.Entry<Long, StreamWrapper> entry : clientStreams.entrySet()) {
                // creates new threads, passing a client's socket stream
                // and saves each returned Future
                futures.add(executor.submit(new GetRequestThread(entry.getValue())));
            }

            List<Car> cars = new ArrayList<>();
            // adds returned Car results of threads
            // to a list
            for (Future<Car> future : futures) {
                try {
                    cars.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            return cars;
        }
        throw new NoSuchElementException();
    }

    public void updateCar(long id, Car car) {
        PrintStream ps = clientStreams.get(id).printStream;
        // sends a PATCH message to client process
        // with attribute to be updated
        // and new value
        ps.println("PATCH DIRECTION " + car.getDirection());
    }

    public void deleteCar(long id) {
        PrintStream ps = clientStreams.get(id).printStream;
        // removes socket's stream from map
        clientStreams.remove(id);
        // sends an EXIT message to client process
        ps.println("EXIT");
    }

    public void deleteCars() {
        int length = clientStreams.size();
        if (length != 0) {
            ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(length);

            for (Map.Entry<Long, StreamWrapper> entry : clientStreams.entrySet()) {
                // creates new threads, passing a client's socket stream
                executor.execute(new DeleteRequestThread(entry.getValue()));
            }
            // deletes socket streams
            clientStreams.clear();
        }
    }
}
